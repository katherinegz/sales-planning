connection: "bq_connector_for_adi"

# include all the views
include: "/views/**/*.view"
include: "/dashboard/*.dashboard.lookml"

datagroup: sales_planning_intern_default_datagroup {
  # sql_trigger: SELECT MAX(id) FROM etl_log;;
  max_cache_age: "1 hour"
}

persist_with: sales_planning_intern_default_datagroup

explore: final {
  join: lat_long {
    relationship: many_to_one
    type: left_outer
    sql_on: ${final.city} = ${lat_long.city};;
  }
}

explore: lat_long {}

explore: pdrb {}

explore: var_importance {}

explore: score {}

map_layer: province_map {
  url: "https://raw.githubusercontent.com/ghapsara/indonesia-atlas/master/provinsi/provinces-simplified-topo.json" # or use the file subparameter
}
