- dashboard: sales_planning_intern_province
  title: Sales Planning Intern - Province
  layout: newspaper
  preferred_viewer: dashboards-next
  description: ''
  filters_location_top: false
  preferred_slug: xo6hBTjjvCG0Ags45F6A4E
  elements:
  - title: Potential Score by Province
    name: Potential Score by Province
    model: sales_planning_intern
    explore: final
    type: looker_grid
    fields: [final.province, average_of_potential_score_next_year]
    fill_fields: [final.province]
    sorts: [average_of_potential_score_next_year desc 0]
    limit: 500
    dynamic_fields: [{category: measure, expression: '', label: Average of Potential
          Score Next Year, value_format: !!null '', value_format_name: decimal_2,
        based_on: final.potential_score_next_year, _kind_hint: measure, measure: average_of_potential_score_next_year,
        type: average, _type_hint: number}]
    show_view_names: false
    show_row_numbers: true
    transpose: false
    truncate_text: true
    hide_totals: false
    hide_row_totals: false
    size_to_fit: true
    table_theme: white
    limit_displayed_rows: false
    enable_conditional_formatting: false
    header_text_alignment: left
    header_font_size: '12'
    rows_font_size: '12'
    conditional_formatting_include_totals: false
    conditional_formatting_include_nulls: false
    show_sql_query_menu_options: false
    show_totals: true
    show_row_totals: true
    truncate_header: false
    series_labels:
      average_of_potential_score_next_year: Potential Score
    series_cell_visualizations:
      average_of_potential_score_next_year:
        is_active: true
        palette:
          palette_id: b7801810-d5f4-d5f9-3898-98b5c9719e48
          collection_id: 7c56cc21-66e4-41c9-81ce-a60e1c3967b2
          custom_colors:
          - "#8cbeff"
          - "#00285c"
    series_text_format:
      average_of_potential_score_next_year:
        align: left
      final.province: {}
    header_background_color: "#DCE6F2"
    series_types: {}
    defaults_version: 1
    listen:
      Province: final.province
    row: 1
    col: 12
    width: 10
    height: 5
  - title: Scoring Map by Province
    name: Scoring Map by Province
    model: sales_planning_intern
    explore: final
    type: looker_map
    fields: [potential_score, final.province]
    fill_fields: [final.province]
    sorts: [potential_score desc 0]
    limit: 500
    dynamic_fields: [{category: measure, expression: '', label: Potential Score, value_format: !!null '',
        value_format_name: decimal_2, based_on: final.potential_score_next_year, _kind_hint: measure,
        measure: potential_score, type: average, _type_hint: number}]
    map_plot_mode: points
    heatmap_gridlines: true
    heatmap_gridlines_empty: false
    heatmap_opacity: 0.7
    show_region_field: true
    draw_map_labels_above_data: true
    map_tile_provider: light_no_labels
    map_position: custom
    map_scale_indicator: 'off'
    map_pannable: false
    map_zoomable: false
    map_marker_type: circle
    map_marker_icon_name: default
    map_marker_radius_mode: proportional_value
    map_marker_units: meters
    map_marker_proportional_scale_type: linear
    map_marker_color_mode: fixed
    show_view_names: false
    show_legend: true
    quantize_map_value_colors: false
    reverse_map_value_colors: false
    map_latitude: -7.491667
    map_longitude: 110.004444
    map_zoom: 6
    map_value_colors: ["#ffffff", "#2b70cc", "#00285c"]
    map_value_scale_clamp_min: 0
    map_value_scale_clamp_max: 100
    map: auto
    map_projection: ''
    quantize_colors: false
    colors: ["#8EB4E3", "#10253F"]
    series_types: {}
    defaults_version: 1
    hidden_fields: []
    hidden_points_if_no: []
    series_labels: {}
    note_state: expanded
    note_display: above
    note_text: click anywhere in the colored map to view by city
    listen:
      Province: final.province
    row: 1
    col: 0
    width: 12
    height: 10
  - title: Top 5 Important Variables
    name: Top 5 Important Variables
    model: sales_planning_intern
    explore: var_importance
    type: looker_grid
    fields: [var_importance.variable, average_of_abs_shap_value_national, value]
    sorts: [average_of_abs_shap_value_national desc 0]
    limit: 500
    dynamic_fields: [{measure: average_of_abs_shap_value_national, based_on: var_importance.abs_shap_value__national_,
        expression: '', label: 'Average of Abs Shap Value National ', type: average,
        _kind_hint: measure, _type_hint: number}, {category: measure, expression: '',
        label: Value, value_format: !!null '', value_format_name: decimal_2, based_on: var_importance.value,
        _kind_hint: measure, measure: value, type: average, _type_hint: number}]
    show_view_names: false
    show_row_numbers: true
    transpose: false
    truncate_text: true
    hide_totals: false
    hide_row_totals: false
    size_to_fit: true
    table_theme: white
    limit_displayed_rows: true
    enable_conditional_formatting: false
    header_text_alignment: left
    header_font_size: '12'
    rows_font_size: '12'
    conditional_formatting_include_totals: false
    conditional_formatting_include_nulls: false
    show_sql_query_menu_options: false
    show_totals: true
    show_row_totals: true
    truncate_header: false
    series_cell_visualizations:
      average_of_value:
        is_active: false
    limit_displayed_rows_values:
      show_hide: show
      first_last: first
      num_rows: '5'
    header_background_color: "#DCE6F2"
    x_axis_gridlines: false
    y_axis_gridlines: true
    show_y_axis_labels: true
    show_y_axis_ticks: true
    y_axis_tick_density: default
    y_axis_tick_density_custom: 5
    show_x_axis_label: true
    show_x_axis_ticks: true
    y_axis_scale_mode: linear
    x_axis_reversed: false
    y_axis_reversed: false
    plot_size_by_field: false
    trellis: ''
    stacking: ''
    legend_position: center
    point_style: none
    show_value_labels: false
    label_density: 25
    x_axis_scale: auto
    y_axis_combined: true
    ordering: none
    show_null_labels: false
    show_totals_labels: false
    show_silhouette: false
    totals_color: "#808080"
    defaults_version: 1
    hidden_fields: [average_of_abs_shap_value_national]
    series_types: {}
    listen:
      Province: var_importance.province
    row: 6
    col: 12
    width: 10
    height: 5
  - type: button
    name: button_270
    rich_content_json: '{"text":"View Comparison Chart","description":"View Comparison
      Chart","newTab":true,"alignment":"left","size":"medium","style":"FILLED","color":"#1A73E8","href":"https://astradigital.cloud.looker.com/dashboards/52?City+A=&Province+A=&Year+A=2020&City+B=&Province+B=&Year+B=2020"}'
    row: 0
    col: 0
    width: 24
    height: 1
  filters:
  - name: Province
    title: Province
    type: field_filter
    default_value: ''
    allow_multiple_values: true
    required: false
    ui_config:
      type: checkboxes
      display: popover
    model: sales_planning_intern
    explore: final
    listens_to_filters: []
    field: final.province
