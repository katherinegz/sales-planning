- dashboard: sales_planning_intern_comparison
  title: Sales Planning Intern - Comparison
  layout: newspaper
  preferred_viewer: dashboards-next
  description: ''
  filters_location_top: false
  preferred_slug: VJKaIN6AXyOvbW2QbQQUoK
  elements:
  - title: Potential Score Next Year
    name: Potential Score Next Year
    model: sales_planning_intern
    explore: final
    type: single_value
    fields: [average_of_potential_score_next_year]
    filters: {}
    limit: 500
    dynamic_fields: [{measure: average_of_potential_score_next_year, based_on: final.potential_score_next_year,
        expression: '', label: Average of Potential Score Next Year, type: average,
        _kind_hint: measure, _type_hint: number}]
    custom_color_enabled: true
    show_single_value_title: true
    show_comparison: false
    comparison_type: value
    comparison_reverse_colors: false
    show_comparison_label: true
    enable_conditional_formatting: false
    conditional_formatting_include_totals: false
    conditional_formatting_include_nulls: false
    custom_color: "#245ab7"
    value_format: "#.00"
    series_types: {}
    defaults_version: 1
    listen:
      Province A: final.province
      City A: final.city
      Year A: final.year
    row: 3
    col: 7
    width: 5
    height: 2
  - title: PDRB ADHK per Sector
    name: PDRB ADHK per Sector
    model: sales_planning_intern
    explore: pdrb
    type: looker_column
    fields: [pdrb.pdrb, average_of_value]
    sorts: [pdrb.pdrb]
    limit: 500
    dynamic_fields: [{category: measure, expression: '', label: Average of Value,
        value_format: !!null '', value_format_name: decimal_0, based_on: pdrb.value,
        _kind_hint: measure, measure: average_of_value, type: average, _type_hint: number}]
    x_axis_gridlines: false
    y_axis_gridlines: true
    show_view_names: false
    show_y_axis_labels: true
    show_y_axis_ticks: true
    y_axis_tick_density: default
    y_axis_tick_density_custom: 5
    show_x_axis_label: true
    show_x_axis_ticks: true
    y_axis_scale_mode: linear
    x_axis_reversed: false
    y_axis_reversed: false
    plot_size_by_field: false
    trellis: ''
    stacking: ''
    limit_displayed_rows: false
    legend_position: center
    point_style: none
    show_value_labels: true
    label_density: 25
    x_axis_scale: auto
    y_axis_combined: true
    ordering: none
    show_null_labels: false
    show_totals_labels: false
    show_silhouette: false
    totals_color: "#808080"
    color_application:
      collection_id: 7c56cc21-66e4-41c9-81ce-a60e1c3967b2
      custom:
        id: 6e27d032-e937-f7c6-e949-762cad08655a
        label: Custom
        type: discrete
        colors:
        - "#245ab7"
        - "#12B5CB"
        - "#E52592"
        - "#E8710A"
        - "#F9AB00"
        - "#7CB342"
        - "#9334E6"
        - "#80868B"
        - "#079c98"
        - "#A8A116"
        - "#EA4335"
        - "#FF8168"
      options:
        steps: 5
    y_axes: [{label: '', orientation: left, series: [{axisId: average_of_value, id: average_of_value,
            name: Value}], showLabels: false, showValues: false, maxValue: 130000000000000,
        minValue: 0, unpinAxis: false, tickDensity: default, tickDensityCustom: 5,
        type: linear}]
    x_axis_label: PDRB Sector
    x_axis_zoom: true
    y_axis_zoom: false
    hide_legend: false
    font_size: '12'
    label_value_format: '#,###.00,,, "B"'
    series_types: {}
    series_labels:
      average_of_value: Value
      pdrb.pdrb: PDRB
    x_axis_label_rotation: 0
    show_row_numbers: true
    truncate_column_names: true
    hide_totals: false
    hide_row_totals: false
    table_theme: white
    enable_conditional_formatting: true
    conditional_formatting_include_totals: false
    conditional_formatting_include_nulls: false
    conditional_formatting: [{type: along a scale..., value: !!null '', background_color: "#1A73E8",
        font_color: !!null '', color_application: {collection_id: 7c56cc21-66e4-41c9-81ce-a60e1c3967b2,
          custom: {id: d2b952c4-21f0-5ce0-c483-6a6f980760fa, label: Custom, type: continuous,
            stops: [{color: "#cde2ff", offset: 0}, {color: "#1c61e8", offset: 100}]},
          options: {steps: 5}}, bold: false, italic: false, strikethrough: false,
        fields: !!null ''}]
    show_null_points: true
    interpolation: linear
    show_sql_query_menu_options: false
    show_totals: true
    show_row_totals: true
    transpose: false
    truncate_text: true
    truncate_header: false
    size_to_fit: true
    series_cell_visualizations:
      average_of_value:
        is_active: true
        palette:
          palette_id: 56d0c358-10a0-4fd6-aa0b-b117bef527ab
          collection_id: 7c56cc21-66e4-41c9-81ce-a60e1c3967b2
    header_text_alignment: left
    header_font_size: '12'
    rows_font_size: '12'
    series_value_format:
      average_of_value: '#.00,,, "B"'
    defaults_version: 1
    value_labels: legend
    label_type: labPer
    groupBars: true
    labelSize: 10pt
    showLegend: true
    up_color: false
    down_color: false
    total_color: false
    leftAxisLabelVisible: false
    leftAxisLabel: ''
    rightAxisLabelVisible: false
    rightAxisLabel: ''
    smoothedBars: false
    orientation: automatic
    labelPosition: left
    percentType: total
    percentPosition: inline
    valuePosition: right
    labelColorEnabled: false
    labelColor: "#FFF"
    listen:
      Province A: pdrb.province
      City A: pdrb.city
      Year A: pdrb.year
    row: 20
    col: 0
    width: 12
    height: 8
  - title: City or District
    name: City or District
    model: sales_planning_intern
    explore: final
    type: single_value
    fields: [final.city]
    sorts: [final.city]
    limit: 500
    custom_color_enabled: true
    show_single_value_title: true
    show_comparison: false
    comparison_type: value
    comparison_reverse_colors: false
    show_comparison_label: true
    enable_conditional_formatting: false
    conditional_formatting_include_totals: false
    conditional_formatting_include_nulls: false
    custom_color: "#245ab7"
    x_axis_gridlines: false
    y_axis_gridlines: true
    show_view_names: false
    show_y_axis_labels: true
    show_y_axis_ticks: true
    y_axis_tick_density: default
    y_axis_tick_density_custom: 5
    show_x_axis_label: true
    show_x_axis_ticks: true
    y_axis_scale_mode: linear
    x_axis_reversed: false
    y_axis_reversed: false
    plot_size_by_field: false
    trellis: ''
    stacking: ''
    limit_displayed_rows: false
    legend_position: center
    point_style: none
    show_value_labels: false
    label_density: 25
    x_axis_scale: auto
    y_axis_combined: true
    ordering: none
    show_null_labels: false
    show_totals_labels: false
    show_silhouette: false
    totals_color: "#808080"
    defaults_version: 1
    series_types: {}
    listen:
      Province A: final.province
      City A: final.city
      Year A: final.year
    row: 5
    col: 0
    width: 12
    height: 2
  - title: Top 5 Important Features
    name: Top 5 Important Features
    model: sales_planning_intern
    explore: var_importance
    type: looker_grid
    fields: [var_importance.variable, average_of_abs_shap_value_city]
    sorts: [average_of_abs_shap_value_city desc]
    limit: 500
    dynamic_fields: [{measure: average_of_abs_shap_value_national, based_on: var_importance.abs_shap_value__national_,
        expression: '', label: 'Average of Abs Shap Value National ', type: average,
        _kind_hint: measure, _type_hint: number}, {category: measure, expression: '',
        label: Average of Value, value_format: !!null '', value_format_name: decimal_2,
        based_on: var_importance.value, _kind_hint: measure, measure: average_of_value,
        type: average, _type_hint: number}, {measure: average_of_abs_shap_value_city,
        based_on: var_importance.abs_shap_value__city_, expression: '', label: 'Average
          of Abs Shap Value City ', type: average, _kind_hint: measure, _type_hint: number}]
    show_view_names: false
    show_row_numbers: true
    transpose: false
    truncate_text: true
    hide_totals: false
    hide_row_totals: false
    size_to_fit: true
    table_theme: white
    limit_displayed_rows: true
    enable_conditional_formatting: false
    header_text_alignment: left
    header_font_size: '12'
    rows_font_size: '12'
    conditional_formatting_include_totals: false
    conditional_formatting_include_nulls: false
    show_sql_query_menu_options: false
    show_totals: true
    show_row_totals: true
    truncate_header: false
    series_labels:
      average_of_value: Value
      var_importance.variable: Feature Name
    series_cell_visualizations:
      average_of_value:
        is_active: false
    series_text_format:
      average_of_value:
        align: left
    limit_displayed_rows_values:
      show_hide: show
      first_last: first
      num_rows: '5'
    header_font_color: "#ffff"
    header_background_color: "#245ab7"
    x_axis_gridlines: false
    y_axis_gridlines: true
    show_y_axis_labels: true
    show_y_axis_ticks: true
    y_axis_tick_density: default
    y_axis_tick_density_custom: 5
    show_x_axis_label: true
    show_x_axis_ticks: true
    y_axis_scale_mode: linear
    x_axis_reversed: false
    y_axis_reversed: false
    plot_size_by_field: false
    trellis: ''
    stacking: ''
    legend_position: center
    point_style: none
    show_value_labels: false
    label_density: 25
    x_axis_scale: auto
    y_axis_combined: true
    ordering: none
    show_null_labels: false
    show_totals_labels: false
    show_silhouette: false
    totals_color: "#808080"
    defaults_version: 1
    series_types: {}
    hidden_fields: [average_of_abs_shap_value_city]
    hidden_pivots: {}
    listen:
      Province A: var_importance.province
      City A: var_importance.city
      Year A: var_importance.year
    row: 7
    col: 0
    width: 12
    height: 5
  - title: Province
    name: Province
    model: sales_planning_intern
    explore: final
    type: single_value
    fields: [final.province]
    fill_fields: [final.province]
    limit: 500
    custom_color_enabled: true
    show_single_value_title: true
    show_comparison: false
    comparison_type: value
    comparison_reverse_colors: false
    show_comparison_label: true
    enable_conditional_formatting: false
    conditional_formatting_include_totals: false
    conditional_formatting_include_nulls: false
    custom_color: "#245ab7"
    series_types: {}
    defaults_version: 1
    listen:
      Province A: final.province
      City A: final.city
      Year A: final.year
    row: 3
    col: 0
    width: 7
    height: 2
  - title: City or District
    name: City or District (2)
    model: sales_planning_intern
    explore: final
    type: single_value
    fields: [final.city]
    sorts: [final.city]
    limit: 500
    custom_color_enabled: true
    show_single_value_title: true
    show_comparison: false
    comparison_type: value
    comparison_reverse_colors: false
    show_comparison_label: true
    enable_conditional_formatting: false
    conditional_formatting_include_totals: false
    conditional_formatting_include_nulls: false
    custom_color: "#569ce1"
    x_axis_gridlines: false
    y_axis_gridlines: true
    show_view_names: false
    show_y_axis_labels: true
    show_y_axis_ticks: true
    y_axis_tick_density: default
    y_axis_tick_density_custom: 5
    show_x_axis_label: true
    show_x_axis_ticks: true
    y_axis_scale_mode: linear
    x_axis_reversed: false
    y_axis_reversed: false
    plot_size_by_field: false
    trellis: ''
    stacking: ''
    limit_displayed_rows: false
    legend_position: center
    point_style: none
    show_value_labels: false
    label_density: 25
    x_axis_scale: auto
    y_axis_combined: true
    ordering: none
    show_null_labels: false
    show_totals_labels: false
    show_silhouette: false
    totals_color: "#808080"
    defaults_version: 1
    series_types: {}
    listen:
      Province B: final.province
      City B: final.city
      Year B: final.year
    row: 5
    col: 12
    width: 12
    height: 2
  - title: Province
    name: Province (2)
    model: sales_planning_intern
    explore: final
    type: single_value
    fields: [final.province]
    fill_fields: [final.province]
    limit: 500
    custom_color_enabled: true
    show_single_value_title: true
    show_comparison: false
    comparison_type: value
    comparison_reverse_colors: false
    show_comparison_label: true
    enable_conditional_formatting: false
    conditional_formatting_include_totals: false
    conditional_formatting_include_nulls: false
    custom_color: "#569ce1"
    series_types: {}
    defaults_version: 1
    listen:
      Province B: final.province
      City B: final.city
      Year B: final.year
    row: 3
    col: 12
    width: 7
    height: 2
  - title: Top 5 Important Features
    name: Top 5 Important Features (2)
    model: sales_planning_intern
    explore: var_importance
    type: looker_grid
    fields: [var_importance.variable, average_of_abs_shap_value_city]
    sorts: [average_of_abs_shap_value_city desc]
    limit: 500
    dynamic_fields: [{measure: average_of_abs_shap_value_national, based_on: var_importance.abs_shap_value__national_,
        expression: '', label: 'Average of Abs Shap Value National ', type: average,
        _kind_hint: measure, _type_hint: number}, {category: measure, expression: '',
        label: Average of Value, value_format: !!null '', value_format_name: decimal_2,
        based_on: var_importance.value, _kind_hint: measure, measure: average_of_value,
        type: average, _type_hint: number}, {measure: average_of_abs_shap_value_city,
        based_on: var_importance.abs_shap_value__city_, expression: '', label: 'Average
          of Abs Shap Value City ', type: average, _kind_hint: measure, _type_hint: number}]
    show_view_names: false
    show_row_numbers: true
    transpose: false
    truncate_text: true
    hide_totals: false
    hide_row_totals: false
    size_to_fit: true
    table_theme: white
    limit_displayed_rows: true
    enable_conditional_formatting: false
    header_text_alignment: left
    header_font_size: '12'
    rows_font_size: '12'
    conditional_formatting_include_totals: false
    conditional_formatting_include_nulls: false
    show_sql_query_menu_options: false
    show_totals: true
    show_row_totals: true
    truncate_header: false
    series_labels:
      average_of_value: Value
      var_importance.variable: Feature Name
    series_cell_visualizations:
      average_of_value:
        is_active: false
    series_text_format:
      average_of_value:
        align: left
    limit_displayed_rows_values:
      show_hide: show
      first_last: first
      num_rows: '5'
    header_font_color: "#ffffff"
    header_background_color: "#569ce1"
    x_axis_gridlines: false
    y_axis_gridlines: true
    show_y_axis_labels: true
    show_y_axis_ticks: true
    y_axis_tick_density: default
    y_axis_tick_density_custom: 5
    show_x_axis_label: true
    show_x_axis_ticks: true
    y_axis_scale_mode: linear
    x_axis_reversed: false
    y_axis_reversed: false
    plot_size_by_field: false
    trellis: ''
    stacking: ''
    legend_position: center
    point_style: none
    show_value_labels: false
    label_density: 25
    x_axis_scale: auto
    y_axis_combined: true
    ordering: none
    show_null_labels: false
    show_totals_labels: false
    show_silhouette: false
    totals_color: "#808080"
    defaults_version: 1
    series_types: {}
    hidden_fields: [average_of_abs_shap_value_city]
    hidden_pivots: {}
    listen:
      Province B: var_importance.province
      City B: var_importance.city
      Year B: var_importance.year
    row: 7
    col: 12
    width: 12
    height: 5
  - title: Potential Score Next Year
    name: Potential Score Next Year (2)
    model: sales_planning_intern
    explore: final
    type: single_value
    fields: [average_of_potential_score_next_year]
    limit: 500
    dynamic_fields: [{measure: average_of_potential_score_next_year, based_on: final.potential_score_next_year,
        expression: '', label: Average of Potential Score Next Year, type: average,
        _kind_hint: measure, _type_hint: number}]
    custom_color_enabled: true
    show_single_value_title: true
    show_comparison: false
    comparison_type: value
    comparison_reverse_colors: false
    show_comparison_label: true
    enable_conditional_formatting: false
    conditional_formatting_include_totals: false
    conditional_formatting_include_nulls: false
    custom_color: "#569ce1"
    value_format: "#.00"
    series_types: {}
    defaults_version: 1
    listen:
      Province B: final.province
      City B: final.city
      Year B: final.year
    row: 3
    col: 19
    width: 5
    height: 2
  - name: ''
    type: text
    title_text: ''
    subtitle_text: ''
    body_text: '[{"type":"p","children":[{"text":""}],"id":1675916545434},{"type":"h1","children":[{"text":"Details","underline":true}],"align":"center"}]'
    rich_content_json: '{"format":"slate"}'
    row: 12
    col: 0
    width: 24
    height: 2
  - title: PDRB ADHK per Sector
    name: PDRB ADHK per Sector (2)
    model: sales_planning_intern
    explore: pdrb
    type: looker_column
    fields: [pdrb.pdrb, average_of_value]
    sorts: [pdrb.pdrb]
    limit: 500
    dynamic_fields: [{category: measure, expression: '', label: Average of Value,
        value_format: !!null '', value_format_name: decimal_0, based_on: pdrb.value,
        _kind_hint: measure, measure: average_of_value, type: average, _type_hint: number}]
    x_axis_gridlines: false
    y_axis_gridlines: true
    show_view_names: false
    show_y_axis_labels: true
    show_y_axis_ticks: true
    y_axis_tick_density: default
    y_axis_tick_density_custom: 5
    show_x_axis_label: true
    show_x_axis_ticks: true
    y_axis_scale_mode: linear
    x_axis_reversed: false
    y_axis_reversed: false
    plot_size_by_field: false
    trellis: ''
    stacking: ''
    limit_displayed_rows: false
    legend_position: center
    point_style: none
    show_value_labels: true
    label_density: 25
    x_axis_scale: auto
    y_axis_combined: true
    ordering: none
    show_null_labels: false
    show_totals_labels: false
    show_silhouette: false
    totals_color: "#808080"
    color_application:
      collection_id: 7c56cc21-66e4-41c9-81ce-a60e1c3967b2
      custom:
        id: 31e7622d-4fb3-d53a-261e-08b0b6f299be
        label: Custom
        type: discrete
        colors:
        - "#569ce1"
        - "#12B5CB"
        - "#E52592"
        - "#E8710A"
        - "#F9AB00"
        - "#7CB342"
        - "#9334E6"
        - "#80868B"
        - "#079c98"
        - "#A8A116"
        - "#EA4335"
        - "#FF8168"
      options:
        steps: 5
    y_axes: [{label: '', orientation: left, series: [{axisId: average_of_value, id: average_of_value,
            name: Value}], showLabels: false, showValues: false, maxValue: 130000000000000,
        minValue: 0, unpinAxis: false, tickDensity: default, tickDensityCustom: 5,
        type: linear}]
    x_axis_label: PDRB Sector
    x_axis_zoom: true
    y_axis_zoom: false
    font_size: '12'
    label_value_format: '#,###.00,,, "B"'
    series_types: {}
    series_labels:
      average_of_value: Value
      pdrb.pdrb: PDRB
    x_axis_label_rotation: 0
    show_row_numbers: true
    truncate_column_names: true
    hide_totals: false
    hide_row_totals: false
    table_theme: white
    enable_conditional_formatting: true
    conditional_formatting_include_totals: false
    conditional_formatting_include_nulls: false
    conditional_formatting: [{type: along a scale..., value: !!null '', background_color: "#1A73E8",
        font_color: !!null '', color_application: {collection_id: 7c56cc21-66e4-41c9-81ce-a60e1c3967b2,
          custom: {id: d2b952c4-21f0-5ce0-c483-6a6f980760fa, label: Custom, type: continuous,
            stops: [{color: "#cde2ff", offset: 0}, {color: "#1c61e8", offset: 100}]},
          options: {steps: 5}}, bold: false, italic: false, strikethrough: false,
        fields: !!null ''}]
    show_null_points: true
    interpolation: linear
    show_sql_query_menu_options: false
    show_totals: true
    show_row_totals: true
    transpose: false
    truncate_text: true
    truncate_header: false
    size_to_fit: true
    series_cell_visualizations:
      average_of_value:
        is_active: true
        palette:
          palette_id: 56d0c358-10a0-4fd6-aa0b-b117bef527ab
          collection_id: 7c56cc21-66e4-41c9-81ce-a60e1c3967b2
    header_text_alignment: left
    header_font_size: '12'
    rows_font_size: '12'
    series_value_format:
      average_of_value: '#.00,,, "B"'
    defaults_version: 1
    value_labels: legend
    label_type: labPer
    groupBars: true
    labelSize: 10pt
    showLegend: true
    up_color: false
    down_color: false
    total_color: false
    leftAxisLabelVisible: false
    leftAxisLabel: ''
    rightAxisLabelVisible: false
    rightAxisLabel: ''
    smoothedBars: false
    orientation: automatic
    labelPosition: left
    percentType: total
    percentPosition: inline
    valuePosition: right
    labelColorEnabled: false
    labelColor: "#FFF"
    listen:
      Province B: pdrb.province
      City B: pdrb.city
      Year B: pdrb.year
    row: 20
    col: 12
    width: 12
    height: 8
  - title: Population Density
    name: Population Density
    model: sales_planning_intern
    explore: final
    type: single_value
    fields: [average_of_population_density]
    limit: 500
    dynamic_fields: [{category: measure, expression: '', label: Average of Motorcycle
          Density, value_format: !!null '', value_format_name: decimal_2, based_on: final.motorcycle_density,
        _kind_hint: measure, measure: average_of_motorcycle_density, type: average,
        _type_hint: number}, {category: measure, expression: '', label: Average of
          Population Density, value_format: !!null '', value_format_name: decimal_2,
        based_on: final.population_density, _kind_hint: measure, measure: average_of_population_density,
        type: average, _type_hint: number}]
    custom_color_enabled: true
    show_single_value_title: true
    show_comparison: false
    comparison_type: value
    comparison_reverse_colors: false
    show_comparison_label: true
    enable_conditional_formatting: false
    conditional_formatting_include_totals: false
    conditional_formatting_include_nulls: false
    custom_color: "#245ab7"
    series_types: {}
    defaults_version: 1
    hidden_pivots: {}
    note_state: collapsed
    note_display: hover
    note_text: Number of population per square meter
    listen:
      Province A: final.province
      City A: final.city
      Year A: final.year
    row: 14
    col: 6
    width: 6
    height: 2
  - title: Motorcycle Density
    name: Motorcycle Density
    model: sales_planning_intern
    explore: final
    type: single_value
    fields: [average_of_motorcycle_density]
    limit: 500
    dynamic_fields: [{category: measure, expression: '', label: Average of Motorcycle
          Density, value_format: !!null '', value_format_name: decimal_2, based_on: final.motorcycle_density,
        _kind_hint: measure, measure: average_of_motorcycle_density, type: average,
        _type_hint: number}]
    custom_color_enabled: true
    show_single_value_title: true
    show_comparison: false
    comparison_type: value
    comparison_reverse_colors: false
    show_comparison_label: true
    enable_conditional_formatting: false
    conditional_formatting_include_totals: false
    conditional_formatting_include_nulls: false
    custom_color: "#245ab7"
    conditional_formatting: [{type: equal to, value: !!null '', background_color: "#1A73E8",
        font_color: !!null '', color_application: {collection_id: 7c56cc21-66e4-41c9-81ce-a60e1c3967b2,
          palette_id: 56d0c358-10a0-4fd6-aa0b-b117bef527ab}, bold: false, italic: false,
        strikethrough: false, fields: !!null ''}]
    series_types: {}
    defaults_version: 1
    note_state: collapsed
    note_display: hover
    note_text: Number of motorcycles per square meter
    listen:
      Province A: final.province
      City A: final.city
      Year A: final.year
    row: 14
    col: 0
    width: 6
    height: 2
  - title: Motorcycle Ownership
    name: Motorcycle Ownership
    model: sales_planning_intern
    explore: final
    type: single_value
    fields: [average_of_motorcycle_ownership]
    limit: 500
    dynamic_fields: [{category: measure, expression: '', label: Average of Motorcycle
          Density, value_format: !!null '', value_format_name: decimal_2, based_on: final.motorcycle_density,
        _kind_hint: measure, measure: average_of_motorcycle_density, type: average,
        _type_hint: number}, {category: measure, expression: '', label: Average of
          Motorcycle Ownership, value_format: !!null '', value_format_name: decimal_2,
        based_on: final.motorcycle_ownership, _kind_hint: measure, measure: average_of_motorcycle_ownership,
        type: average, _type_hint: number}]
    custom_color_enabled: true
    show_single_value_title: true
    show_comparison: false
    comparison_type: value
    comparison_reverse_colors: false
    show_comparison_label: true
    enable_conditional_formatting: false
    conditional_formatting_include_totals: false
    conditional_formatting_include_nulls: false
    custom_color: "#245ab7"
    value_format: '#.00 "%"'
    series_types: {}
    defaults_version: 1
    hidden_pivots: {}
    note_state: collapsed
    note_display: hover
    note_text: Percentage of households that owns at least one motorcycle
    listen:
      Province A: final.province
      City A: final.city
      Year A: final.year
    row: 16
    col: 0
    width: 4
    height: 2
  - title: PDRB per Capita
    name: PDRB per Capita
    model: sales_planning_intern
    explore: final
    type: single_value
    fields: [average_of_pdrb_adhk_per_capita_by_province]
    limit: 500
    dynamic_fields: [{category: measure, expression: '', label: Average of Motorcycle
          Density, value_format: !!null '', value_format_name: decimal_2, based_on: final.motorcycle_density,
        _kind_hint: measure, measure: average_of_motorcycle_density, type: average,
        _type_hint: number}, {category: measure, expression: '', label: 'Average of
          Pdrb Adhk per Capita By Province ', value_format: !!null '', value_format_name: decimal_2,
        based_on: final.pdrb_adhk_per_capita__by_province_, _kind_hint: measure, measure: average_of_pdrb_adhk_per_capita_by_province,
        type: average, _type_hint: number}]
    custom_color_enabled: true
    show_single_value_title: true
    show_comparison: false
    comparison_type: value
    comparison_reverse_colors: false
    show_comparison_label: true
    enable_conditional_formatting: false
    conditional_formatting_include_totals: false
    conditional_formatting_include_nulls: false
    custom_color: "#245ab7"
    value_format: '#,###.00,, "M"'
    series_types: {}
    defaults_version: 1
    hidden_pivots: {}
    note_state: collapsed
    note_display: hover
    note_text: Gross value added per person (Measured by province)
    listen:
      Province A: final.province
      City A: final.city
      Year A: final.year
    row: 18
    col: 0
    width: 4
    height: 2
  - title: House Ownership
    name: House Ownership
    model: sales_planning_intern
    explore: final
    type: single_value
    fields: [average_of_house_ownership]
    limit: 500
    dynamic_fields: [{category: measure, expression: '', label: Average of Motorcycle
          Density, value_format: !!null '', value_format_name: decimal_2, based_on: final.motorcycle_density,
        _kind_hint: measure, measure: average_of_motorcycle_density, type: average,
        _type_hint: number}, {category: measure, expression: '', label: Average of
          Motorcycle Ownership, value_format: !!null '', value_format_name: decimal_2,
        based_on: final.motorcycle_ownership, _kind_hint: measure, measure: average_of_motorcycle_ownership,
        type: average, _type_hint: number}, {category: measure, expression: '', label: Average
          of House Ownership, value_format: !!null '', value_format_name: decimal_2,
        based_on: final.house_ownership, _kind_hint: measure, measure: average_of_house_ownership,
        type: average, _type_hint: number}]
    custom_color_enabled: true
    show_single_value_title: true
    show_comparison: false
    comparison_type: value
    comparison_reverse_colors: false
    show_comparison_label: true
    enable_conditional_formatting: false
    conditional_formatting_include_totals: false
    conditional_formatting_include_nulls: false
    custom_color: "#245ab7"
    value_format: '#.00 "%"'
    series_types: {}
    defaults_version: 1
    hidden_pivots: {}
    note_state: collapsed
    note_display: hover
    note_text: Percentage of households that has a house ownership
    listen:
      Province A: final.province
      City A: final.city
      Year A: final.year
    row: 16
    col: 4
    width: 4
    height: 2
  - title: Internet Access
    name: Internet Access
    model: sales_planning_intern
    explore: final
    type: single_value
    fields: [average_of_internet_access]
    limit: 500
    dynamic_fields: [{category: measure, expression: '', label: Average of Motorcycle
          Density, value_format: !!null '', value_format_name: decimal_2, based_on: final.motorcycle_density,
        _kind_hint: measure, measure: average_of_motorcycle_density, type: average,
        _type_hint: number}, {category: measure, expression: '', label: Average of
          Motorcycle Ownership, value_format: !!null '', value_format_name: decimal_2,
        based_on: final.motorcycle_ownership, _kind_hint: measure, measure: average_of_motorcycle_ownership,
        type: average, _type_hint: number}, {category: measure, expression: '', label: Average
          of Internet Access, value_format: !!null '', value_format_name: decimal_2,
        based_on: final.internet_access, _kind_hint: measure, measure: average_of_internet_access,
        type: average, _type_hint: number}]
    custom_color_enabled: true
    show_single_value_title: true
    show_comparison: false
    comparison_type: value
    comparison_reverse_colors: false
    show_comparison_label: true
    enable_conditional_formatting: false
    conditional_formatting_include_totals: false
    conditional_formatting_include_nulls: false
    custom_color: "#245ab7"
    value_format: '#.00 "%"'
    series_types: {}
    defaults_version: 1
    hidden_pivots: {}
    note_state: collapsed
    note_display: hover
    note_text: Percentage of households that has access to the internet
    listen:
      Province A: final.province
      City A: final.city
      Year A: final.year
    row: 16
    col: 8
    width: 4
    height: 2
  - title: Hourly Wage
    name: Hourly Wage
    model: sales_planning_intern
    explore: final
    type: single_value
    fields: [average_of_hourly_wage_by_province]
    limit: 500
    dynamic_fields: [{category: measure, expression: '', label: Average of Motorcycle
          Density, value_format: !!null '', value_format_name: decimal_2, based_on: final.motorcycle_density,
        _kind_hint: measure, measure: average_of_motorcycle_density, type: average,
        _type_hint: number}, {category: measure, expression: '', label: 'Average of
          Pdrb Adhk per Capita By Province ', value_format: !!null '', value_format_name: decimal_2,
        based_on: final.pdrb_adhk_per_capita__by_province_, _kind_hint: measure, measure: average_of_pdrb_adhk_per_capita_by_province,
        type: average, _type_hint: number}, {measure: average_of_hourly_wage_by_province,
        based_on: final.hourly_wage__by_province_, expression: '', label: 'Average
          of Hourly Wage By Province ', type: average, _kind_hint: measure, _type_hint: number}]
    custom_color_enabled: true
    show_single_value_title: true
    show_comparison: false
    comparison_type: value
    comparison_reverse_colors: false
    show_comparison_label: true
    enable_conditional_formatting: false
    conditional_formatting_include_totals: false
    conditional_formatting_include_nulls: false
    custom_color: "#245ab7"
    value_format: "#,###.00"
    series_types: {}
    defaults_version: 1
    hidden_pivots: {}
    note_state: collapsed
    note_display: hover
    note_text: Hourly earned wage (Measured by province)
    listen:
      Province A: final.province
      City A: final.city
      Year A: final.year
    row: 18
    col: 4
    width: 4
    height: 2
  - title: Poverty Line
    name: Poverty Line
    model: sales_planning_intern
    explore: final
    type: single_value
    fields: [average_of_poverty_line]
    limit: 500
    dynamic_fields: [{category: measure, expression: '', label: Average of Motorcycle
          Density, value_format: !!null '', value_format_name: decimal_2, based_on: final.motorcycle_density,
        _kind_hint: measure, measure: average_of_motorcycle_density, type: average,
        _type_hint: number}, {measure: average_of_poverty_line, based_on: final.poverty_line,
        expression: '', label: Average of Poverty Line, type: average, _kind_hint: measure,
        _type_hint: number}]
    custom_color_enabled: true
    show_single_value_title: true
    show_comparison: false
    comparison_type: value
    comparison_reverse_colors: false
    show_comparison_label: true
    enable_conditional_formatting: false
    conditional_formatting_include_totals: false
    conditional_formatting_include_nulls: false
    custom_color: "#245ab7"
    value_format: '#,###.00, "K"'
    series_types: {}
    defaults_version: 1
    hidden_pivots: {}
    note_state: collapsed
    note_display: hover
    note_text: The bare minimum of expenditure required by a person to meet their
      basic needs for one month
    listen:
      Province A: final.province
      City A: final.city
      Year A: final.year
    row: 18
    col: 8
    width: 4
    height: 2
  - title: Motorcycle Density
    name: Motorcycle Density (2)
    model: sales_planning_intern
    explore: final
    type: single_value
    fields: [average_of_motorcycle_density]
    limit: 500
    dynamic_fields: [{category: measure, expression: '', label: Average of Motorcycle
          Density, value_format: !!null '', value_format_name: decimal_2, based_on: final.motorcycle_density,
        _kind_hint: measure, measure: average_of_motorcycle_density, type: average,
        _type_hint: number}]
    custom_color_enabled: true
    show_single_value_title: true
    show_comparison: false
    comparison_type: value
    comparison_reverse_colors: false
    show_comparison_label: true
    enable_conditional_formatting: false
    conditional_formatting_include_totals: false
    conditional_formatting_include_nulls: false
    custom_color: "#569ce1"
    series_types: {}
    defaults_version: 1
    note_state: collapsed
    note_display: hover
    note_text: Number of motorcycles per square meter
    listen:
      Province B: final.province
      City B: final.city
      Year B: final.year
    row: 14
    col: 12
    width: 6
    height: 2
  - title: Population Density
    name: Population Density (2)
    model: sales_planning_intern
    explore: final
    type: single_value
    fields: [average_of_population_density]
    limit: 500
    dynamic_fields: [{category: measure, expression: '', label: Average of Motorcycle
          Density, value_format: !!null '', value_format_name: decimal_2, based_on: final.motorcycle_density,
        _kind_hint: measure, measure: average_of_motorcycle_density, type: average,
        _type_hint: number}, {category: measure, expression: '', label: Average of
          Population Density, value_format: !!null '', value_format_name: decimal_2,
        based_on: final.population_density, _kind_hint: measure, measure: average_of_population_density,
        type: average, _type_hint: number}]
    custom_color_enabled: true
    show_single_value_title: true
    show_comparison: false
    comparison_type: value
    comparison_reverse_colors: false
    show_comparison_label: true
    enable_conditional_formatting: false
    conditional_formatting_include_totals: false
    conditional_formatting_include_nulls: false
    custom_color: "#569ce1"
    series_types: {}
    defaults_version: 1
    hidden_pivots: {}
    note_state: collapsed
    note_display: hover
    note_text: Number of population per square meter
    listen:
      Province B: final.province
      City B: final.city
      Year B: final.year
    row: 14
    col: 18
    width: 6
    height: 2
  - title: Motorcycle Ownership
    name: Motorcycle Ownership (2)
    model: sales_planning_intern
    explore: final
    type: single_value
    fields: [average_of_motorcycle_ownership]
    limit: 500
    dynamic_fields: [{category: measure, expression: '', label: Average of Motorcycle
          Density, value_format: !!null '', value_format_name: decimal_2, based_on: final.motorcycle_density,
        _kind_hint: measure, measure: average_of_motorcycle_density, type: average,
        _type_hint: number}, {category: measure, expression: '', label: Average of
          Motorcycle Ownership, value_format: !!null '', value_format_name: decimal_2,
        based_on: final.motorcycle_ownership, _kind_hint: measure, measure: average_of_motorcycle_ownership,
        type: average, _type_hint: number}]
    custom_color_enabled: true
    show_single_value_title: true
    show_comparison: false
    comparison_type: value
    comparison_reverse_colors: false
    show_comparison_label: true
    enable_conditional_formatting: false
    conditional_formatting_include_totals: false
    conditional_formatting_include_nulls: false
    custom_color: "#569ce1"
    value_format: '#.00 "%"'
    series_types: {}
    defaults_version: 1
    hidden_pivots: {}
    note_state: collapsed
    note_display: hover
    note_text: Percentage of households that owns at least one motorcycle
    listen:
      Province B: final.province
      City B: final.city
      Year B: final.year
    row: 16
    col: 12
    width: 4
    height: 2
  - title: House Ownership
    name: House Ownership (2)
    model: sales_planning_intern
    explore: final
    type: single_value
    fields: [average_of_house_ownership]
    limit: 500
    dynamic_fields: [{category: measure, expression: '', label: Average of Motorcycle
          Density, value_format: !!null '', value_format_name: decimal_2, based_on: final.motorcycle_density,
        _kind_hint: measure, measure: average_of_motorcycle_density, type: average,
        _type_hint: number}, {category: measure, expression: '', label: Average of
          Motorcycle Ownership, value_format: !!null '', value_format_name: decimal_2,
        based_on: final.motorcycle_ownership, _kind_hint: measure, measure: average_of_motorcycle_ownership,
        type: average, _type_hint: number}, {category: measure, expression: '', label: Average
          of House Ownership, value_format: !!null '', value_format_name: decimal_2,
        based_on: final.house_ownership, _kind_hint: measure, measure: average_of_house_ownership,
        type: average, _type_hint: number}]
    custom_color_enabled: true
    show_single_value_title: true
    show_comparison: false
    comparison_type: value
    comparison_reverse_colors: false
    show_comparison_label: true
    enable_conditional_formatting: false
    conditional_formatting_include_totals: false
    conditional_formatting_include_nulls: false
    custom_color: "#569ce1"
    value_format: '#.00 "%"'
    series_types: {}
    defaults_version: 1
    hidden_pivots: {}
    note_state: collapsed
    note_display: above
    note_text: Percentage of households that has a house ownership
    listen:
      Province B: final.province
      City B: final.city
      Year B: final.year
    row: 16
    col: 16
    width: 4
    height: 2
  - title: Internet Access
    name: Internet Access (2)
    model: sales_planning_intern
    explore: final
    type: single_value
    fields: [average_of_internet_access]
    limit: 500
    dynamic_fields: [{category: measure, expression: '', label: Average of Motorcycle
          Density, value_format: !!null '', value_format_name: decimal_2, based_on: final.motorcycle_density,
        _kind_hint: measure, measure: average_of_motorcycle_density, type: average,
        _type_hint: number}, {category: measure, expression: '', label: Average of
          Motorcycle Ownership, value_format: !!null '', value_format_name: decimal_2,
        based_on: final.motorcycle_ownership, _kind_hint: measure, measure: average_of_motorcycle_ownership,
        type: average, _type_hint: number}, {category: measure, expression: '', label: Average
          of Internet Access, value_format: !!null '', value_format_name: decimal_2,
        based_on: final.internet_access, _kind_hint: measure, measure: average_of_internet_access,
        type: average, _type_hint: number}]
    custom_color_enabled: true
    show_single_value_title: true
    show_comparison: false
    comparison_type: value
    comparison_reverse_colors: false
    show_comparison_label: true
    enable_conditional_formatting: false
    conditional_formatting_include_totals: false
    conditional_formatting_include_nulls: false
    custom_color: "#569ce1"
    value_format: '#.00 "%"'
    series_types: {}
    defaults_version: 1
    hidden_pivots: {}
    note_state: collapsed
    note_display: hover
    note_text: Percentage of households that has access to the internet
    listen:
      Province B: final.province
      City B: final.city
      Year B: final.year
    row: 16
    col: 20
    width: 4
    height: 2
  - title: PDRB per Capita
    name: PDRB per Capita (2)
    model: sales_planning_intern
    explore: final
    type: single_value
    fields: [average_of_pdrb_adhk_per_capita_by_province]
    limit: 500
    dynamic_fields: [{category: measure, expression: '', label: Average of Motorcycle
          Density, value_format: !!null '', value_format_name: decimal_2, based_on: final.motorcycle_density,
        _kind_hint: measure, measure: average_of_motorcycle_density, type: average,
        _type_hint: number}, {category: measure, expression: '', label: 'Average of
          Pdrb Adhk per Capita By Province ', value_format: !!null '', value_format_name: decimal_2,
        based_on: final.pdrb_adhk_per_capita__by_province_, _kind_hint: measure, measure: average_of_pdrb_adhk_per_capita_by_province,
        type: average, _type_hint: number}]
    custom_color_enabled: true
    show_single_value_title: true
    show_comparison: false
    comparison_type: value
    comparison_reverse_colors: false
    show_comparison_label: true
    enable_conditional_formatting: false
    conditional_formatting_include_totals: false
    conditional_formatting_include_nulls: false
    custom_color: "#569ce1"
    value_format: '#,###.00,, "M"'
    series_types: {}
    defaults_version: 1
    hidden_pivots: {}
    note_state: collapsed
    note_display: hover
    note_text: Gross value added per person (Measured by province)
    listen:
      Province B: final.province
      City B: final.city
      Year B: final.year
    row: 18
    col: 12
    width: 4
    height: 2
  - title: Hourly Wage
    name: Hourly Wage (2)
    model: sales_planning_intern
    explore: final
    type: single_value
    fields: [average_of_hourly_wage_by_province]
    limit: 500
    dynamic_fields: [{category: measure, expression: '', label: Average of Motorcycle
          Density, value_format: !!null '', value_format_name: decimal_2, based_on: final.motorcycle_density,
        _kind_hint: measure, measure: average_of_motorcycle_density, type: average,
        _type_hint: number}, {category: measure, expression: '', label: 'Average of
          Pdrb Adhk per Capita By Province ', value_format: !!null '', value_format_name: decimal_2,
        based_on: final.pdrb_adhk_per_capita__by_province_, _kind_hint: measure, measure: average_of_pdrb_adhk_per_capita_by_province,
        type: average, _type_hint: number}, {measure: average_of_hourly_wage_by_province,
        based_on: final.hourly_wage__by_province_, expression: '', label: 'Average
          of Hourly Wage By Province ', type: average, _kind_hint: measure, _type_hint: number}]
    custom_color_enabled: true
    show_single_value_title: true
    show_comparison: false
    comparison_type: value
    comparison_reverse_colors: false
    show_comparison_label: true
    enable_conditional_formatting: false
    conditional_formatting_include_totals: false
    conditional_formatting_include_nulls: false
    custom_color: "#569ce1"
    value_format: "#,###.00"
    series_types: {}
    defaults_version: 1
    hidden_pivots: {}
    note_state: collapsed
    note_display: hover
    note_text: Hourly earned wage (Measured by province)
    listen:
      Province B: final.province
      City B: final.city
      Year B: final.year
    row: 18
    col: 16
    width: 4
    height: 2
  - title: Poverty Line
    name: Poverty Line (2)
    model: sales_planning_intern
    explore: final
    type: single_value
    fields: [average_of_poverty_line]
    limit: 500
    dynamic_fields: [{category: measure, expression: '', label: Average of Motorcycle
          Density, value_format: !!null '', value_format_name: decimal_2, based_on: final.motorcycle_density,
        _kind_hint: measure, measure: average_of_motorcycle_density, type: average,
        _type_hint: number}, {measure: average_of_poverty_line, based_on: final.poverty_line,
        expression: '', label: Average of Poverty Line, type: average, _kind_hint: measure,
        _type_hint: number}]
    custom_color_enabled: true
    show_single_value_title: true
    show_comparison: false
    comparison_type: value
    comparison_reverse_colors: false
    show_comparison_label: true
    enable_conditional_formatting: false
    conditional_formatting_include_totals: false
    conditional_formatting_include_nulls: false
    custom_color: "#569ce1"
    value_format: '#,###.00, "K"'
    series_types: {}
    defaults_version: 1
    hidden_pivots: {}
    note_state: collapsed
    note_display: hover
    note_text: The bare minimum of expenditure required by a person to meet their
      basic needs for one month
    listen:
      Province B: final.province
      City B: final.city
      Year B: final.year
    row: 18
    col: 20
    width: 4
    height: 2
  - name: " (2)"
    type: text
    title_text: ''
    subtitle_text: ''
    body_text: '[{"type":"h1","children":[{"text":"Option A","underline":true,"color":"hsl(218,
      67%, 43%)"}],"align":"center"}]'
    rich_content_json: '{"format":"slate"}'
    row: 1
    col: 0
    width: 12
    height: 2
  - name: " (Copy)"
    type: text
    title_text: " (Copy)"
    subtitle_text: ''
    body_text: '[{"type":"h1","children":[{"text":"Option B","underline":true,"color":"hsl(210,
      70%, 61%)"}],"align":"center"}]'
    rich_content_json: '{"format":"slate"}'
    row: 1
    col: 12
    width: 12
    height: 2
  - type: button
    name: button_271
    rich_content_json: '{"text":"View City Page","description":"View City Page","newTab":true,"alignment":"left","size":"medium","style":"FILLED","color":"#1A73E8","href":"https://astradigital.cloud.looker.com/dashboards/37?Province="}'
    row: 0
    col: 5
    width: 11
    height: 1
  - type: button
    name: button_272
    rich_content_json: '{"text":"View Province Page","description":"View Province
      Page","newTab":true,"alignment":"center","size":"medium","style":"FILLED","color":"#1A73E8","href":"https://astradigital.cloud.looker.com/dashboards/36?Province="}'
    row: 0
    col: 0
    width: 5
    height: 1
  filters:
  - name: Province A
    title: Province A
    type: field_filter
    default_value: ''
    allow_multiple_values: true
    required: false
    ui_config:
      type: radio_buttons
      display: popover
    model: sales_planning_intern
    explore: final
    listens_to_filters: []
    field: final.province
  - name: City A
    title: City A
    type: field_filter
    default_value: ''
    allow_multiple_values: true
    required: false
    ui_config:
      type: radio_buttons
      display: popover
    model: sales_planning_intern
    explore: final
    listens_to_filters: [Province A]
    field: final.city
  - name: Year A
    title: Year A
    type: field_filter
    default_value: '2020'
    allow_multiple_values: true
    required: false
    ui_config:
      type: slider
      display: inline
      options:
        min: 2019
        max: 2021
    model: sales_planning_intern
    explore: final
    listens_to_filters: []
    field: final.year
  - name: Province B
    title: Province B
    type: field_filter
    default_value: ''
    allow_multiple_values: true
    required: false
    ui_config:
      type: radio_buttons
      display: popover
    model: sales_planning_intern
    explore: final
    listens_to_filters: []
    field: final.province
  - name: City B
    title: City B
    type: field_filter
    default_value: ''
    allow_multiple_values: true
    required: false
    ui_config:
      type: radio_buttons
      display: popover
    model: sales_planning_intern
    explore: final
    listens_to_filters: [Province B]
    field: final.city
  - name: Year B
    title: Year B
    type: field_filter
    default_value: '2020'
    allow_multiple_values: true
    required: false
    ui_config:
      type: slider
      display: inline
      options:
        min: 2019
        max: 2021
    model: sales_planning_intern
    explore: final
    listens_to_filters: []
    field: final.year
