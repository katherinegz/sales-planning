- dashboard: sales_planning_intern_details
  title: Sales Planning Intern - Details
  layout: newspaper
  preferred_viewer: dashboards-next
  description: ''
  preferred_slug: WakihqX0HgKdul3hG08FFK
  elements:
  - title: Potential Score Next Year
    name: Potential Score Next Year
    model: sales_planning_intern
    explore: final
    type: single_value
    fields: [average_of_potential_score_next_year]
    limit: 500
    dynamic_fields: [{measure: average_of_potential_score_next_year, based_on: final.potential_score_next_year,
        expression: '', label: Average of Potential Score Next Year, type: average,
        _kind_hint: measure, _type_hint: number}]
    custom_color_enabled: true
    show_single_value_title: true
    show_comparison: false
    comparison_type: value
    comparison_reverse_colors: false
    show_comparison_label: true
    enable_conditional_formatting: false
    conditional_formatting_include_totals: false
    conditional_formatting_include_nulls: false
    value_format: "#.00"
    series_types: {}
    defaults_version: 1
    listen:
      Province: final.province
      City: final.city
      Year: final.year
    row: 0
    col: 15
    width: 7
    height: 2
  - title: PDRB ADHK per Sector
    name: PDRB ADHK per Sector
    model: sales_planning_intern
    explore: pdrb
    type: table
    fields: [pdrb.pdrb, average_of_value]
    sorts: [average_of_value desc]
    limit: 500
    dynamic_fields: [{category: measure, expression: '', label: Average of Value,
        value_format: !!null '', value_format_name: decimal_0, based_on: pdrb.value,
        _kind_hint: measure, measure: average_of_value, type: average, _type_hint: number}]
    show_view_names: false
    show_row_numbers: true
    truncate_column_names: true
    hide_totals: false
    hide_row_totals: false
    table_theme: white
    limit_displayed_rows: false
    enable_conditional_formatting: true
    conditional_formatting_include_totals: false
    conditional_formatting_include_nulls: false
    color_application: undefined
    series_labels:
      average_of_value: Value
      pdrb.pdrb: PDRB
    conditional_formatting: [{type: along a scale..., value: !!null '', background_color: "#1A73E8",
        font_color: !!null '', color_application: {collection_id: 7c56cc21-66e4-41c9-81ce-a60e1c3967b2,
          custom: {id: d2b952c4-21f0-5ce0-c483-6a6f980760fa, label: Custom, type: continuous,
            stops: [{color: "#cde2ff", offset: 0}, {color: "#1c61e8", offset: 100}]},
          options: {steps: 5}}, bold: false, italic: false, strikethrough: false,
        fields: !!null ''}]
    x_axis_gridlines: false
    y_axis_gridlines: true
    y_axes: [{label: '', orientation: left, series: [{axisId: average_of_value, id: average_of_value,
            name: Value}], showLabels: false, showValues: false, unpinAxis: false,
        tickDensity: default, tickDensityCustom: 5, type: linear}]
    show_y_axis_labels: true
    show_y_axis_ticks: true
    y_axis_tick_density: default
    y_axis_tick_density_custom: 5
    show_x_axis_label: false
    show_x_axis_ticks: true
    y_axis_scale_mode: linear
    x_axis_reversed: false
    y_axis_reversed: false
    plot_size_by_field: false
    x_axis_zoom: true
    y_axis_zoom: false
    trellis: ''
    stacking: ''
    legend_position: center
    font_size: 12
    label_value_format: '#.00,,, "B"'
    series_types: {}
    point_style: none
    show_value_labels: true
    label_density: 25
    x_axis_scale: auto
    y_axis_combined: true
    x_axis_label_rotation: 0
    show_null_points: true
    interpolation: linear
    show_totals_labels: false
    show_silhouette: false
    totals_color: "#808080"
    ordering: none
    show_null_labels: false
    show_sql_query_menu_options: false
    show_totals: true
    show_row_totals: true
    transpose: false
    truncate_text: true
    truncate_header: false
    size_to_fit: true
    series_cell_visualizations:
      average_of_value:
        is_active: true
        palette:
          palette_id: 56d0c358-10a0-4fd6-aa0b-b117bef527ab
          collection_id: 7c56cc21-66e4-41c9-81ce-a60e1c3967b2
    header_text_alignment: left
    header_font_size: '12'
    rows_font_size: '12'
    series_value_format:
      average_of_value: '#.00,,, "B"'
    defaults_version: 1
    value_labels: legend
    label_type: labPer
    groupBars: true
    labelSize: 10pt
    showLegend: true
    up_color: false
    down_color: false
    total_color: false
    leftAxisLabelVisible: false
    leftAxisLabel: ''
    rightAxisLabelVisible: false
    rightAxisLabel: ''
    smoothedBars: false
    orientation: automatic
    labelPosition: left
    percentType: total
    percentPosition: inline
    valuePosition: right
    labelColorEnabled: false
    labelColor: "#FFF"
    listen:
      Province: pdrb.province
      City: pdrb.city
      Year: pdrb.year
    row: 2
    col: 11
    width: 11
    height: 6
  - title: City or District
    name: City or District
    model: sales_planning_intern
    explore: final
    type: single_value
    fields: [final.city]
    sorts: [final.city]
    limit: 500
    custom_color_enabled: true
    show_single_value_title: true
    show_comparison: false
    comparison_type: value
    comparison_reverse_colors: false
    show_comparison_label: true
    enable_conditional_formatting: false
    conditional_formatting_include_totals: false
    conditional_formatting_include_nulls: false
    x_axis_gridlines: false
    y_axis_gridlines: true
    show_view_names: false
    show_y_axis_labels: true
    show_y_axis_ticks: true
    y_axis_tick_density: default
    y_axis_tick_density_custom: 5
    show_x_axis_label: true
    show_x_axis_ticks: true
    y_axis_scale_mode: linear
    x_axis_reversed: false
    y_axis_reversed: false
    plot_size_by_field: false
    trellis: ''
    stacking: ''
    limit_displayed_rows: false
    legend_position: center
    point_style: none
    show_value_labels: false
    label_density: 25
    x_axis_scale: auto
    y_axis_combined: true
    ordering: none
    show_null_labels: false
    show_totals_labels: false
    show_silhouette: false
    totals_color: "#808080"
    defaults_version: 1
    series_types: {}
    listen:
      Province: final.province
      City: final.city
      Year: final.year
    row: 0
    col: 6
    width: 9
    height: 2
  - title: Top 5 Important Features
    name: Top 5 Important Features
    model: sales_planning_intern
    explore: var_importance
    type: looker_grid
    fields: [var_importance.variable, average_of_value, average_of_abs_shap_value_city]
    sorts: [average_of_abs_shap_value_city desc]
    limit: 500
    dynamic_fields: [{measure: average_of_abs_shap_value_national, based_on: var_importance.abs_shap_value__national_,
        expression: '', label: 'Average of Abs Shap Value National ', type: average,
        _kind_hint: measure, _type_hint: number}, {category: measure, expression: '',
        label: Average of Value, value_format: !!null '', value_format_name: decimal_2,
        based_on: var_importance.value, _kind_hint: measure, measure: average_of_value,
        type: average, _type_hint: number}, {measure: average_of_abs_shap_value_city,
        based_on: var_importance.abs_shap_value__city_, expression: '', label: 'Average
          of Abs Shap Value City ', type: average, _kind_hint: measure, _type_hint: number}]
    show_view_names: false
    show_row_numbers: true
    transpose: false
    truncate_text: true
    hide_totals: false
    hide_row_totals: false
    size_to_fit: true
    table_theme: white
    limit_displayed_rows: true
    enable_conditional_formatting: false
    header_text_alignment: left
    header_font_size: '12'
    rows_font_size: '12'
    conditional_formatting_include_totals: false
    conditional_formatting_include_nulls: false
    show_sql_query_menu_options: false
    show_totals: true
    show_row_totals: true
    truncate_header: false
    series_labels:
      average_of_value: Value
      var_importance.variable: Feature Name
    series_cell_visualizations:
      average_of_value:
        is_active: false
    series_text_format:
      average_of_value:
        align: left
    limit_displayed_rows_values:
      show_hide: show
      first_last: first
      num_rows: '5'
    header_background_color: "#DCE6F2"
    x_axis_gridlines: false
    y_axis_gridlines: true
    show_y_axis_labels: true
    show_y_axis_ticks: true
    y_axis_tick_density: default
    y_axis_tick_density_custom: 5
    show_x_axis_label: true
    show_x_axis_ticks: true
    y_axis_scale_mode: linear
    x_axis_reversed: false
    y_axis_reversed: false
    plot_size_by_field: false
    trellis: ''
    stacking: ''
    legend_position: center
    point_style: none
    show_value_labels: false
    label_density: 25
    x_axis_scale: auto
    y_axis_combined: true
    ordering: none
    show_null_labels: false
    show_totals_labels: false
    show_silhouette: false
    totals_color: "#808080"
    defaults_version: 1
    series_types: {}
    hidden_fields: [average_of_abs_shap_value_city]
    hidden_pivots: {}
    listen:
      Province: var_importance.province
      City: var_importance.city
      Year: var_importance.year
    row: 2
    col: 0
    width: 11
    height: 6
  - title: Province
    name: Province
    model: sales_planning_intern
    explore: final
    type: single_value
    fields: [final.province]
    fill_fields: [final.province]
    limit: 500
    custom_color_enabled: true
    show_single_value_title: true
    show_comparison: false
    comparison_type: value
    comparison_reverse_colors: false
    show_comparison_label: true
    enable_conditional_formatting: false
    conditional_formatting_include_totals: false
    conditional_formatting_include_nulls: false
    series_types: {}
    defaults_version: 1
    listen:
      Province: final.province
      City: final.city
      Year: final.year
    row: 0
    col: 0
    width: 6
    height: 2
  filters:
  - name: Province
    title: Province
    type: field_filter
    default_value: ''
    allow_multiple_values: true
    required: false
    ui_config:
      type: radio_buttons
      display: popover
    model: sales_planning_intern
    explore: final
    listens_to_filters: []
    field: final.province
  - name: City
    title: City
    type: field_filter
    default_value: ''
    allow_multiple_values: true
    required: false
    ui_config:
      type: radio_buttons
      display: popover
    model: sales_planning_intern
    explore: final
    listens_to_filters: [Province]
    field: final.city
  - name: Year
    title: Year
    type: field_filter
    default_value: '2021'
    allow_multiple_values: true
    required: false
    ui_config:
      type: dropdown_menu
      display: inline
      options:
      - '2019'
      - '2020'
      - '2021'
    model: sales_planning_intern
    explore: final
    listens_to_filters: []
    field: final.year
