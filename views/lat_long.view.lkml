view: lat_long {
  sql_table_name: `astra-self-service.sales_planning_intern.lat_long`
    ;;

  dimension: city {
    type: string
    sql: ${TABLE}.City ;;
  }

  dimension: latitude {
    type: number
    sql: ${TABLE}.Latitude ;;
  }

  dimension: longitude {
    type: number
    sql: ${TABLE}.Longitude ;;
  }

  dimension: location {
    type: location
    sql_longitude: ${longitude};;
    sql_latitude: ${latitude};;
  }

  dimension: province {
    type: string
    case: {
      when: {
        sql: ${TABLE}.Province = "DKI Jakarta" ;;
        label: "Jakarta"
      }
      when: {
        sql: ${TABLE}.Province = "DI Yogyakarta" ;;
        label: "Yogyakarta"
      }
      when: {
        sql: ${TABLE}.Province = "Banten" ;;
        label: "Banten"
      }
      when: {
        sql: ${TABLE}.Province = "Jawa Barat" ;;
        label: "Jawa Barat"
      }
      when: {
        sql: ${TABLE}.Province = "Jawa Tengah" ;;
        label: "Jawa Tengah"
      }
      when: {
        sql: ${TABLE}.Province = "Jawa Timur" ;;
        label: "Jawa Timur"
      }
    }
    sql: ${TABLE}.Province ;;
  }

  measure: count {
    type: count
    drill_fields: []
  }
}
