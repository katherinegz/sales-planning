view: var_importance {
  sql_table_name: `astra-self-service.sales_planning_intern.var_importance`
    ;;

  dimension: abs_shap_value__city_ {
    type: number
    sql: ${TABLE}.Abs_SHAP_Value__City_ ;;
  }

  dimension: abs_shap_value__province_ {
    type: number
    sql: ${TABLE}.Abs_SHAP_Value__Province_ ;;
  }

  dimension: abs_shap_value__national_ {
    type: number
    sql: ${TABLE}.Abs_SHAP_Value__National_ ;;
  }

  dimension: city {
    type: string
    sql: ${TABLE}.City ;;
  }

  dimension: province {
    type: string
    case: {
      when: {
        sql: ${TABLE}.Province = "DKI Jakarta" ;;
        label: "Jakarta"
      }
      when: {
        sql: ${TABLE}.Province = "DI Yogyakarta" ;;
        label: "Yogyakarta"
      }
      when: {
        sql: ${TABLE}.Province = "Banten" ;;
        label: "Banten"
      }
      when: {
        sql: ${TABLE}.Province = "Jawa Barat" ;;
        label: "Jawa Barat"
      }
      when: {
        sql: ${TABLE}.Province = "Jawa Tengah" ;;
        label: "Jawa Tengah"
      }
      when: {
        sql: ${TABLE}.Province = "Jawa Timur" ;;
        label: "Jawa Timur"
      }
    }
    sql: ${TABLE}.Province ;;
  }

  dimension: shap_value {
    type: number
    sql: ${TABLE}.SHAP_Value ;;
  }

  dimension: value {
    type: number
    sql: ${TABLE}.Value ;;
  }

  dimension: variable {
    type: string
    sql: ${TABLE}.Variable ;;
  }

  dimension: year {
    type: number
    value_format:"0"
    sql: ${TABLE}.Year ;;
  }

  measure: count {
    type: count
    drill_fields: []
  }
}
