view: score {
  derived_table: {
    sql: SELECT
        province as Province, city as City, year+1 as Year, potential_score_next_year as Score
      FROM astra-self-service.sales_planning_intern.final
       ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  dimension: province {
    type: string
    case: {
      when: {
        sql: ${TABLE}.Province = "DKI Jakarta" ;;
        label: "Jakarta"
      }
      when: {
        sql: ${TABLE}.Province = "DI Yogyakarta" ;;
        label: "Yogyakarta"
      }
      when: {
        sql: ${TABLE}.Province = "Banten" ;;
        label: "Banten"
      }
      when: {
        sql: ${TABLE}.Province = "Jawa Barat" ;;
        label: "Jawa Barat"
      }
      when: {
        sql: ${TABLE}.Province = "Jawa Tengah" ;;
        label: "Jawa Tengah"
      }
      when: {
        sql: ${TABLE}.Province = "Jawa Timur" ;;
        label: "Jawa Timur"
      }
    }
    sql: ${TABLE}.Province ;;
  }

  dimension: city {
    type: string
    sql: ${TABLE}.City ;;
  }

  dimension: year {
    type: number
    value_format:"0"
    sql: ${TABLE}.Year ;;
  }

  dimension: score {
    type: number
    sql: ${TABLE}.Score ;;
  }

  set: detail {
    fields: [province, city, year, score]
  }
}
