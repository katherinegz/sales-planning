view: final {
  sql_table_name: `astra-self-service.sales_planning_intern.final`
    ;;

  dimension: city {
    type: string
    sql: ${TABLE}.City ;;
    view_label: "Province"
    link: {
      label: "View by Province"
      url: "https://astradigital.cloud.looker.com/dashboards/36?Province={{ _filters['final.province'] | url_encode }}"
    }
  }

  dimension: hourly_wage__by_province_ {
    type: number
    sql: ${TABLE}.Hourly_Wage__by_Province_ ;;
  }

  dimension: house_ownership {
    type: number
    sql: ${TABLE}.House_Ownership ;;
  }

  dimension: internet_access {
    type: number
    sql: ${TABLE}.Internet_Access ;;
  }

  dimension: motorcycle_density {
    type: number
    sql: ${TABLE}.Motorcycle_Density ;;
  }

  dimension: motorcycle_ownership {
    type: number
    sql: ${TABLE}.Motorcycle_Ownership ;;
  }

  dimension: pdrb_adhk_agriculture_forestry_and_fishery {
    type: number
    sql: ${TABLE}.PDRB_ADHK_Agriculture_Forestry_and_Fishery ;;
  }

  dimension: pdrb_adhk_financial_and_insurance_services {
    type: number
    sql: ${TABLE}.PDRB_ADHK_Financial_and_Insurance_Services ;;
  }

  dimension: pdrb_adhk_transportation_and_warehouse {
    type: number
    sql: ${TABLE}.PDRB_ADHK_Transportation_and_Warehouse ;;
  }

  dimension: pdrb_adhk_accommodation_food_and_drink {
    type: number
    sql: ${TABLE}.PDRB_ADHK_Accommodation_Food_and_Drink ;;
  }

  dimension: pdrb_adhk_information_and_communication {
    type: number
    sql: ${TABLE}.PDRB_ADHK_Information_and_Communication ;;
  }

  dimension: pdrb_adhk_per_capita__by_province_ {
    type: number
    sql: ${TABLE}.PDRB_ADHK_Per_Capita__by_Province_ ;;
  }

  dimension: population_density {
    type: number
    sql: ${TABLE}.Population_Density ;;
  }

  dimension: financial_institution {
    type: number
    sql: ${TABLE}.Financial_Institution ;;
  }

  dimension: private_to_public_school_ratio {
    type: number
    sql: ${TABLE}.Private_to_Public_School_Ratio ;;
  }

  dimension: poverty_line {
    type: number
    sql: ${TABLE}.Poverty_Line ;;
  }

  dimension: disaster_frequency {
    type: number
    sql: ${TABLE}.Disaster_Frequency ;;
  }

  dimension: potential_score_next_year {
    type: number
    sql: ${TABLE}.Potential_Score_Next_Year ;;
  }

  dimension: province {
    type: string
    case: {
      when: {
        sql: ${TABLE}.Province = "DKI Jakarta" ;;
        label: "Jakarta"
      }
      when: {
        sql: ${TABLE}.Province = "DI Yogyakarta" ;;
        label: "Yogyakarta"
      }
      when: {
        sql: ${TABLE}.Province = "Banten" ;;
        label: "Banten"
      }
      when: {
        sql: ${TABLE}.Province = "Jawa Barat" ;;
        label: "Jawa Barat"
      }
      when: {
        sql: ${TABLE}.Province = "Jawa Tengah" ;;
        label: "Jawa Tengah"
      }
      when: {
        sql: ${TABLE}.Province = "Jawa Timur" ;;
        label: "Jawa Timur"
      }
    }
    sql: ${TABLE}.Province;;
    map_layer_name: province_map
    view_label: "Province"
    link: {
      label: "View by City"
      url: "https://astradigital.cloud.looker.com/dashboards/37?Province={{ _filters['final.province'] | url_encode }}"
    }
  }

  dimension: year {
    type: number
    value_format:"0"
    sql: ${TABLE}.Year ;;
  }

  measure: count {
    type: count
    drill_fields: []
  }
}
