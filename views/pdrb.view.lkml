view: pdrb {
  sql_table_name: `astra-self-service.sales_planning_intern.pdrb`
    ;;

  dimension: city {
    type: string
    sql: ${TABLE}.City ;;
  }

  dimension: pdrb {
    type: string
    case: {
      when: {
        sql: ${TABLE}.PDRB = "PDRB ADHK Agriculture Forestry and Fishery" ;;
        label: "Agriculture Forestry and Fishery"
      }
      when: {
        sql: ${TABLE}.PDRB = "PDRB ADHK Financial and Insurance Services" ;;
        label: "Financial and Insurance Services"
      }
      when: {
        sql: ${TABLE}.PDRB = "PDRB ADHK Accommodation Food and Drink" ;;
        label: "Accommodation Food and Drink"
      }
      when: {
        sql: ${TABLE}.PDRB = "PDRB ADHK Information and Communication" ;;
        label: "Information and Communication"
      }
      when: {
        sql: ${TABLE}.PDRB = "PDRB ADHK Transportation and Warehouse" ;;
        label: "Transportation and Warehouse"
      }
    }
    sql: ${TABLE}.PDRB ;;
  }

  dimension: province {
    type: string
    case: {
      when: {
        sql: ${TABLE}.Province = "DKI Jakarta" ;;
        label: "Jakarta"
      }
      when: {
        sql: ${TABLE}.Province = "DI Yogyakarta" ;;
        label: "Yogyakarta"
      }
      when: {
        sql: ${TABLE}.Province = "Banten" ;;
        label: "Banten"
      }
      when: {
        sql: ${TABLE}.Province = "Jawa Barat" ;;
        label: "Jawa Barat"
      }
      when: {
        sql: ${TABLE}.Province = "Jawa Tengah" ;;
        label: "Jawa Tengah"
      }
      when: {
        sql: ${TABLE}.Province = "Jawa Timur" ;;
        label: "Jawa Timur"
      }
    }
    sql: ${TABLE}.Province ;;
  }

  dimension: value {
    type: number
    sql: ${TABLE}.Value ;;
  }

  dimension: year {
    type: number
    value_format:"0"
    sql: ${TABLE}.Year ;;
  }

  measure: count {
    type: count
    drill_fields: []
  }
}
